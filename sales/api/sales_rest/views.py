from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import Salesperson, Customer, Sale, AutomobileVO
from django.http import JsonResponse
import json

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]

class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "first_name",
        "last_name",
        "address",
        "phone_number",
    ]

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "automobile",
        "salesperson",
        "customer",
        "price",
    ]
    encoders={
        "automobile":AutomobileVOEncoder(),
        "salesperson":SalespersonEncoder(),
        "customer":CustomerEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople":salespeople},
            encoder=SalespersonEncoder,
        )
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def api_detail_salespeople(request, id):
    if request.method == "DELETE":
        try:
            salesperson = Salesperson.objects.filter(id=id)
            deleted = salesperson.delete()
            return JsonResponse({"deleted":deleted[0] > 0})
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Salesperson does not exist"})
            response.status_code = 404
            return response

@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers":customers},
            encoder=CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def api_detail_customer(request, id):
    if request.method == "DELETE":
        try:
            customer = Customer.objects.filter(id=id)
            deleted = customer.delete()
            return JsonResponse({"deleted":deleted[0] > 0})
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Customer does not exist"})
            response.status_code = 404
            return response

@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales":sales},
            encoder=SaleEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            automobile = AutomobileVO.objects.get(vin=content["automobile"])
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "No automobiles"},
                status=404,
            )
        try:
            salesperson = Salesperson.objects.get(id=content["salesperson"])
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid salesperson id"},
                status=404,
            )
        try:
            customer = Customer.objects.get(id=content["customer"])
        except Customer.DoesNotExist:
            return JsonResponse(
                status=404,
            )

        content["automobile"] = automobile
        content["salesperson"] = salesperson
        content["customer"] = customer
        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def api_detail_sale(request, id):
    if request.method == "DELETE":
        try:
            sale = Sale.objects.filter(id=id)
            deleted = sale.delete()
            return JsonResponse({"deleted":deleted[0] > 0})
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Sales record does not exist"})
            response.status_code = 404
            return response
