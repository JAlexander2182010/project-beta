import React, { useState } from "react";

function AddAutoForm(props) {
  const [ color, setColor ] = useState("");
  const [ year, setYear ] = useState("");
  const [ model, setModel ] = useState("");
  const [ vin, setVin ] = useState("");

  async function handleSubmit(event) {
    event.preventDefault();

    const data = {};
    data.color = color;
    data.year = year;
    data.model_id = model;
    data.vin = vin;

    const url = 'http://localhost:8100/api/automobiles/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      setColor("");
      setYear("");
      setModel("");
      setVin("");
      props.getAutomobiles();
    }

  }

  function handleChangeColor(event) {
    const value = event.target.value;
    setColor(value);
  }

  function handleChangeYear(event) {
    const value = event.target.value;
    setYear(value);
  }

  function handleChangeVin(event) {
    const value = event.target.value;
    setVin(value);
  }

  function handleChangeModel(event) {
    const value = event.target.value;
    setModel(value);
  }

  return (
    <div className="row">
    <div className="offset-3 col-6">
      <div className="shadow p-4 mt-4">
        <h1>Add an Automobile to Inventory</h1>
        <form onSubmit={handleSubmit} id="create-auto">
          <div className="form-floating mb-3">
            <input
              onChange={handleChangeColor}
              value={color}
              placeholder="color"
              required
              type="text"
              name="color"
              id="color"
              className="form-control"
            />
            <label htmlFor="color">Color</label>
          </div>
          <div className="form-floating mb-3">
            <input
              onChange={handleChangeYear}
              value={year}
              placeholder="year"
              required
              type="text"
              name="year"
              id="year"
              className="form-control"
            />
            <label htmlFor="year">Year</label>
          </div>
          <div className="form-floating mb-3">
            <input
              onChange={handleChangeVin}
              value={vin}
              placeholder="vin"
              required
              type="text"
              name="vin"
              id="vin"
              className="form-control"
            />
            <label htmlFor="vin">Vin</label>
          </div>
          <div className="mb-3">
            <select
              onChange={handleChangeModel}
              value={model}
              required
              name="location"
              id="location"
              className="form-select"
            >
              <option value="">Choose a model</option>
              {props.models.map((model) => {
                return (
                  <option key={model.id} value={model.id}>
                    {model.name}
                  </option>
                );
              })}
            </select>
          </div>
          <button className="btn btn-primary">Create</button>
        </form>
      </div>
    </div>
  </div>
  )
}

export default AddAutoForm;
