function AutoInventoryList(props) {

  function booToWord(boolean) {
    if (boolean === true) {
      return "yes";
    }
    return "no";
  }

  return (
    <div>
      <h1>Automobile Inventory</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Vin</th>
            <th>Color</th>
            <th>Year</th>
            <th>Model</th>
            <th>Manufacturer</th>
            <th>Sold</th>
          </tr>
        </thead>
        <tbody>
          {props.automobiles.map((automobile) => {
            return (
              <tr key={automobile.href}>
                <td>{automobile.vin}</td>
                <td>{automobile.color}</td>
                <td>{automobile.year}</td>
                <td>{automobile.model.name}</td>
                <td>{automobile.model.manufacturer.name}</td>
                <td>{booToWord(automobile.sold)}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default AutoInventoryList;
