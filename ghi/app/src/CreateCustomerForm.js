import React, { useState } from 'react';

function CreateCustomerForm(props){

    const [first_name, setFirstName] = useState('')
    const [last_name, setLastName] = useState('')
    const [address, setAddress] = useState('')
    const [phone_number, setPhoneNumber] = useState('')

    async function handleSubmit(event){
        event.preventDefault();
        const data = {
            first_name,
            last_name,
            address,
            phone_number,
        };

        const locationURL = 'http://localhost:8090/api/customers/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationURL, fetchConfig);
        console.log(response)
        if(response.ok){
            const newCustomer = await response.json();
            console.log(newCustomer)

            setFirstName('');
            setLastName('');
            setAddress('');
            setPhoneNumber('');
            props.getCustomers();
        }
    }

    function handleFirstNameChange(event){
        const value = event.target.value;
        setFirstName(value);
    }

    function handleLastNameChange(event){
        const value = event.target.value;
        setLastName(value);
    }

    function handleAddressChange(event){
        const value = event.target.value;
        setAddress(value);
    }

    function handlePhoneNumberChange(event){
        const value = event.target.value;
        setPhoneNumber(value);
    }

    return(
        <div>
            <form onSubmit={handleSubmit} id="create-manufacturer-form">
                <h1>Create a Customer</h1>
                <div className="form-floating mb-3">
                    <input value={first_name} onChange={handleFirstNameChange} placeholder="First Name" required type="text" name="firstName" id="firstName" className="form-control" />
                    <label htmlFor="firstName">First Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={last_name} onChange={handleLastNameChange} placeholder="Last Name" required type="text" name="lastName" id="lastName" className="form-control" />
                    <label htmlFor="lastName">Last Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={address} onChange={handleAddressChange} placeholder="Address" required type="text" name="address" id="address" className="form-control" />
                    <label htmlFor="address">Address</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={phone_number} onChange={handlePhoneNumberChange} placeholder="Phone Number" required type="text" name="phoneNumber" id="phoneNumber" className="form-control" />
                    <label htmlFor="phoneNumber">Phone Number</label>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
        </div>
    )
}

export default CreateCustomerForm
