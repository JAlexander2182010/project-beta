import React, { useState } from 'react';

function SalesHistory(props){
    const [salesperson, setSalesperson] = useState('')


    function handleSalespersonChange(event){
        const value = event.target.value;
        setSalesperson(value)
    }

    const filteredSales = salesperson ? props.sales.filter(
        (sale) => sale.salesperson.id === parseInt(salesperson, 10)
      )
    : [];

    return(
        <div className="mb-3">
            <h1>Salesperson History</h1>
            <label htmlFor="salesperson">Salesperson</label>
              <select onChange={handleSalespersonChange} value={props.salespeople.id} required name="salespeople" id="salesperson" className="form-select">
                <option value="">Choose a salesperson...</option>
                {props.salespeople.map(salesperson => {
                  return (
                    <option key={salesperson.id} value={salesperson.id}>{salesperson.first_name} {salesperson.last_name}</option>
                  )
                })}
              </select>
              <table className="table table-striped">
          <thead>
            <tr>
              <th>Salesperson</th>
              <th>Customer</th>
              <th>VIN</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
            {filteredSales.map((sale) => (
              <tr key={sale.id}>
                <td>
                  {sale.salesperson.first_name} {sale.salesperson.last_name}
                </td>
                <td>
                  {sale.customer.first_name} {sale.customer.last_name}
                </td>
                <td>{sale.automobile.vin}</td>
                <td>${Number(sale.price).toFixed(2)}</td>
              </tr>
            ))}
        </tbody>
        </table>
            </div>
    )
}
export default SalesHistory
