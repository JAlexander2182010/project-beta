import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
                <NavLink className="nav-link" to="/create_manufacturer">Create Manufacturer</NavLink>
                <NavLink className="nav-link" to="/manufacturer/list">Manufacturer List</NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" to="/create_vehicle">Create Model</NavLink>
                <NavLink className="nav-link" to="/list_automobiles">Models</NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" to="/addAuto">Add Automobile to Inventory</NavLink>
                <NavLink className="nav-link" to="/autoInventory">Automobile Inventory List</NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" to="/create_salesperson">Create Salesperson</NavLink>
                <NavLink className="nav-link" to="/listSalespeople">Salespeople</NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" to="/createCustomer">Add Customer</NavLink>
                <NavLink className="nav-link" to="/listCustomers">Customers</NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" to="/salesform">Create Sale</NavLink>
                <NavLink className="nav-link" to="/saleslist">Sales</NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" to="/AddTechnician">Add Technician</NavLink>
                <NavLink className="nav-link" to="/TechnicianList">Technician List</NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" to="/AppointmentForm">Create a Service Appointment</NavLink>
                <NavLink className="nav-link" to="/AppointmentList">Appointment List</NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" to="/saleshistory">Salesperson History</NavLink>
                <NavLink className="nav-link" to="/ServiceHistory">Service History</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
