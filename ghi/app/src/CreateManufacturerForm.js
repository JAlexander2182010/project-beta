import React, { useState } from 'react';

function CreateManufacturerForm(props){
    const [name, setManufacturerName] = useState('');

    async function handleSubmit(event){
        event.preventDefault();
        const data = {
            name,
        };

        const locationURL = 'http://localhost:8100/api/manufacturers/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationURL, fetchConfig);
        if(response.ok){
            setManufacturerName('');
            props.getManufacturerList();
        }
    }

    function handleManufacturerNameChange(event){
        const value = event.target.value;
        setManufacturerName(value);
    }

    return(
        <div>
            <form onSubmit={handleSubmit} id="create-manufacturer-form">
                <h1>Create a manufacturer</h1>
                <div className="form-floating mb-3">
                    <input value={name} onChange={handleManufacturerNameChange} placeholder="Manufacturer Name" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                    <label htmlFor="name">Manufacturer name...</label>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
        </div>
    )
}
export default CreateManufacturerForm;
