function ServiceHistory(props) {

  function IsVip(vin) {
    const matchingVin = props.automobiles.find(auto => auto.vin === vin);
    return matchingVin ? "YES" : "NO";
  }

  return (
    <div>
      <h1>Service History</h1>

      <table className="table table-striped">
          <thead>
            <tr>
              <th>Vin</th>
              <th>Customer Name</th>
              <th>Date</th>
              <th>Time</th>
              <th>Technician</th>
              <th>Reason</th>
              <th>VIP</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            {props.appointments.map((appointment) => {
              const date = appointment.date_time.slice(0, 10);
              const time = appointment.date_time.slice(11, 16);
              const isVip = IsVip(appointment.vin);
              return (
                <tr key={appointment.id}>
                  <td>{appointment.vin}</td>
                  <td>{appointment.customer}</td>
                  <td>{date}</td>
                  <td>{time}</td>
                  <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                  <td>{appointment.reason}</td>
                  <td>{isVip}</td>
                  <td>{appointment.status}</td>
                </tr>
              );
            })}
          </tbody>
        </table>

    </div>
  );
}

export default ServiceHistory;
