import React, { useState, useEffect } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import CreateManufacturerForm from "./CreateManufacturerForm";
import AutomobileList from "./AutomobileList";
import Nav from "./Nav";
import ManufacturerList from "./ManufacturerList";
import CreateVehicle from "./CreateVehicle";
import AddAutoForm from "./AddAutoForm";
import AutoInventoryList from "./AutoInventoryList";
import AddSalespersonForm from "./AddSalespersonForm";
import ListSalespeople from "./ListSalespeople";
import CreateCustomerForm from "./CreateCustomerForm";
import ListCustomers from "./ListCustomers";
import SalesForm from "./SalesForm";
import SalesList from "./SalesList";
import SalesHistory from "./SalesHistory";
import AddTechnicianForm from "./AddTechnicianForm";
import TechnicianList from "./TechnicianList";
import AppointmentForm from "./AppointmentForm";
import AppointmentList from "./AppointmentList";
import ServiceHistory from "./ServiceHistory";

function App() {
  const [manufacturerList, setManufactureList] = useState([]);
  const [automobiles, setAutomobiles] = useState([]);
  const [salespeople, setSalespeople] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [sales, setSales] = useState([]);
  const [technicians, setTechnicians] = useState([]);
  const [appointments, setAppointments] = useState([]);
  const [models, setModels] = useState([]);

  async function getManufacturerList() {
    const url = "http://localhost:8100/api/manufacturers/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setManufactureList(data.manufacturers);
    }
  }

  async function getAutomobiles() {
    const url = "http://localhost:8100/api/automobiles/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.autos);
    }
  }

  async function getSalespeople() {
    const url = "http://localhost:8090/api/salespeople/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setSalespeople(data.salespeople);
    }
  }

  async function getCustomers() {
    const url = "http://localhost:8090/api/customers/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
    }
  }

  async function getSales() {
    const url = "http://localhost:8090/api/sales/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setSales(data.sales);
    }
  }

  async function getTechnicians() {
    const url = "http://localhost:8080/api/technicians/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  }

  async function getAppointments() {
    const url = "http://localhost:8080/api/appointments/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
    }
  }

  async function getAutoModels() {
    const url = "http://localhost:8100/api/models";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
  }

  useEffect(() => {
    getManufacturerList();
    getAutomobiles();
    getSalespeople();
    getCustomers();
    getSales();
    getTechnicians();
    getAppointments();
    getAutoModels();
  }, []);

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route
            path="/create_manufacturer"
            element={
              <CreateManufacturerForm
                getManufacturerList={getManufacturerList}
              />
            }
          />
          <Route
            path="/manufacturer/list"
            element={<ManufacturerList manufacturerList={manufacturerList} />}
          />
          <Route
            path="/list_automobiles"
            element={<AutomobileList models={models} />}
          />
          <Route
            path="/create_vehicle"
            element={
              <CreateVehicle
                manufacturerList={manufacturerList}
                getAutoModels={getAutoModels}
              />
            }
          />
          <Route
            path="/addAuto"
            element={
              <AddAutoForm models={models} getAutomobiles={getAutomobiles} />
            }
          />
          <Route
            path="/autoInventory"
            element={<AutoInventoryList automobiles={automobiles} />}
          />
          <Route
            path="/create_salesperson"
            element={<AddSalespersonForm getSalespeople={getSalespeople} />}
          />
          <Route
            path="/listSalespeople"
            element={<ListSalespeople salespeople={salespeople} />}
          />
          <Route
            path="/createCustomer"
            element={<CreateCustomerForm getCustomers={getCustomers} />}
          />
          <Route
            path="/listCustomers"
            element={<ListCustomers customers={customers} />}
          />
          <Route
            path="/salesform"
            element={
              <SalesForm
                automobiles={automobiles}
                salespeople={salespeople}
                customers={customers}
                getSales={getSales}
              />
            }
          />
          <Route path="/saleslist" element={<SalesList sales={sales} />} />
          <Route
            path="/salesHistory"
            element={<SalesHistory sales={sales} salespeople={salespeople} />}
          />
          <Route
            path="/AddTechnician"
            element={<AddTechnicianForm getTechnicians={getTechnicians} />}
          />
          <Route
            path="/TechnicianList"
            element={<TechnicianList technicians={technicians} />}
          />
          <Route
            path="/AppointmentForm"
            element={
              <AppointmentForm
                technicians={technicians}
                getAppointments={getAppointments}
              />
            }
          />
          <Route
            path="/AppointmentList"
            element={
              <AppointmentList
                appointments={appointments}
                automobiles={automobiles} getAppointments={getAppointments}
              />
            }
          />
          <Route
            path="/ServiceHistory"
            element={
              <ServiceHistory
                appointments={appointments}
                automobiles={automobiles}
                getAppointments={getAppointments}
              />
            }
          />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
