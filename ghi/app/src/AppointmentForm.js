import React, { useState } from "react";

function AppointmentForm(props) {
  const [ vin, setVin ] = useState("");
  const [ customer, setCustomer ] = useState("");
  const [ date, setDate ] = useState("");
  const [ time, setTime ] = useState("");
  const [ reason, setReason ] = useState("");
  const [ isTyping, setIsTyping ] = useState(false);
  const [technician, setTechnician ] = useState("");

  async function handleSubmit(event) {
    event.preventDefault();

    const data = {}
    data.vin = vin;
    data.customer = customer;
    data.date = date;
    data.time = time;
    data.technician = technician;
    data.reason = reason;
    data.vip = false;

    const url = "http://localhost:8080/api/appointments/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      setVin("");
      setCustomer("");
      setDate("");
      setTime("");
      setReason("");
      setIsTyping(false);
      setTechnician("");
      props.getAppointments()
    }

  }

  function handleChangeVin(event) {
    const value = event.target.value;
    setVin(value);
  }

  function handleChangeCustomer(event) {
    const value = event.target.value;
    setCustomer(value);
  }

  function handleChangeDate(event) {
    const value = event.target.value;
    setDate(value);
  }
  function handleChangeTime(event) {
    const value = event.target.value;
    setTime(value);
  }

  function handleChangeTechnician(event) {
    const value = event.target.value;
    setTechnician(value);
  }

  function handleChangeReason(event) {
    const value = event.target.value;
    setIsTyping(true);
    setReason(value);
  }

  return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add an Appointment</h1>
            <form onSubmit={handleSubmit} id="create-appointment">
              <div className="form-floating mb-3">
                <input
                  onChange={handleChangeVin}
                  value={vin}
                  placeholder="vin"
                  required
                  type="text"
                  name="vin"
                  id="vin"
                  className="form-control"
                />
                <label htmlFor="vin">Automobile VIN</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleChangeCustomer}
                  value={customer}
                  placeholder="customer"
                  required
                  type="text"
                  name="customer"
                  id="customer"
                  className="form-control"
                />
                <label htmlFor="customer">Customer</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleChangeDate}
                  value={date}
                  placeholder="date"
                  required
                  type="date"
                  name="date"
                  id="date"
                  className="form-control"
                />
                <label htmlFor="date">Date</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleChangeTime}
                  value={time}
                  placeholder="time"
                  required
                  type="time"
                  name="time"
                  id="time"
                  className="form-control"
                />
                <label htmlFor="time">Time</label>
              </div>
              <div className="mb-3">
                <select
                  onChange={handleChangeTechnician}
                  value={technician}
                  required
                  name="technician"
                  id="technician"
                  className="form-select"
                >
                  <option value="">Choose a Technician</option>
                  {props.technicians.map((technician) => {
                    return (
                      <option key={technician.id} value={technician.id}>
                        {technician.first_name} {technician.last_name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className="form-floating mb-3">
                <textarea
                  onChange={handleChangeReason}
                  value={reason}
                  placeholder="reason"
                  required
                  type="text"
                  name="reason"
                  id="reason"
                  className="form-control"
                />
                {!isTyping && <label htmlFor="reason">Reason for Appointment</label>}
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
  )
}

export default AppointmentForm
