import React, { useState } from 'react';

function AddSalespersonForm(props){

    const [first_name, setFirstName] = useState('')
    const [last_name, setLastName] = useState('')
    const [employee_id, setEmployeeID]  = useState('')

    async function handleSubmit(event){
        event.preventDefault();
        const data = {
            first_name,
            last_name,
            employee_id,
        };

        const locationURL = 'http://localhost:8090/api/salespeople/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationURL, fetchConfig);
        console.log(response)
        if(response.ok){
            const newSalesperson = await response.json();
            console.log(newSalesperson)

            setFirstName('');
            setLastName('');
            setEmployeeID('');
            props.getSalespeople();
        }
    }

    function handleFirstNameChange(event){
        const value = event.target.value;
        setFirstName(value);
    }

    function handleLastNameChange(event){
        const value = event.target.value;
        setLastName(value);
    }

    function handleEmployeeIDChange(event){
        const value = event.target.value;
        setEmployeeID(value);
    }

    return(
        <div>
            <form onSubmit={handleSubmit} id="create-manufacturer-form">
                <h1>Add a Salesperson</h1>
                <div className="form-floating mb-3">
                    <input value={first_name} onChange={handleFirstNameChange} placeholder="First Name" required type="text" name="firstName" id="firstName" className="form-control" />
                    <label htmlFor="firstName">First Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={last_name} onChange={handleLastNameChange} placeholder="Last Name" required type="text" name="lastName" id="lastName" className="form-control" />
                    <label htmlFor="lastName">Last Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={employee_id} onChange={handleEmployeeIDChange} placeholder="Employee ID" required type="text" name="employeeID" id="employeeID" className="form-control" />
                    <label htmlFor="employeeID">Employee ID</label>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
        </div>
    )
}
export default AddSalespersonForm
